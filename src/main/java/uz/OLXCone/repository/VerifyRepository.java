package uz.OLXCone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.OLXCone.model.Verify;

import java.sql.Timestamp;
import java.util.UUID;
@Repository
public interface VerifyRepository extends JpaRepository<Verify, UUID> {
    Verify findByCode(Integer code);
    boolean existsByCode(Integer code);

    void deleteAllByTimeBefore(Timestamp time);
}
